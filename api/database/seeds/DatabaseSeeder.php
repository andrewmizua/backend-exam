<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        factory(App\User::class, 5)->create();
        factory(App\Post::class, 5)->create();

        //factory(App\User::class, 5)->create()->each(function($user) {
            //$user->posts()->saveMany(factory(App\Post::class, 5)->make());
            //$user->posts()->save(factory(App\Post::class)->make());
        //});
    }
}
