<?php

use App\Comment;
use App\Post;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title'   => $faker->sentence(5),
        'content' => $faker->paragraph(4),
        'image'   => $faker->imageUrl(640, 480, 'cats'),
        'user_id' => $faker->randomElement(User::pluck('id')->toArray())
    ];
});

$factory->afterCreating(Post::class, function ($post, $faker) {
    $post->comments()->saveMany(factory(Comment::class, 5)->make());
});
