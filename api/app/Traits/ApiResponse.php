<?php

namespace App\Traits;

use Illuminate\Http\Response;

trait ApiResponse {

    /**
     * Success response
     *
     * @param  array $data
     * @param  int $code
     * @return Illuminate\Http\Response
     */
    public function success($data, $code = Response::HTTP_OK) {
        return response(['data' => $data], $code)->header('Content-Type', 'application/json');
    }

    /**
     * Error response
     *
     * @param  mixed $message
     * @param  int $code
     * @return Illuminate\Http\Response
     */
    public function error($message, $code) {
        return response()->json(['errors' => $message, 'code' => $code], $code);
    }

}

