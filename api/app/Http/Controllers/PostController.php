<?php

namespace App\Http\Controllers;

use App\Post;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{

    use ApiResponse;

    /**
     * View posts
     *
     * @return Illuminate\Http\Response
     */
    public function index() {
        return response()->json(Post::orderBy('created_at', 'desc')->paginate(), Response::HTTP_OK);
    }

    /**
     * View post
     *
     * @param  Post   $post
     * @return Illuminate\Http\Response
     */
    public function view(Post $post) {

        return $this->success($post, Response::HTTP_OK);

    }

    /**
     * Create post
     *
     * @param  Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request) {

        $data = $request->validate(['title' => 'required', 'content' => 'required', 'image' => 'required']);

        $data['user_id'] = Auth::user()->id;

        return $this->success(Post::create($data), Response::HTTP_CREATED);

    }

    /**
     * Update post
     *
     * @param  Post    $post
     * @param  Request $request
     * @return Illuminate\Http\Response
     */
    public function update(Post $post, Request $request) {

        $data = $request->validate(['title' => 'required', 'content' => 'required', 'image' => 'required']);
        $post->fill($data)->save();

        return $this->success($post, Response::HTTP_OK);

    }

    /**
     * Delete post
     *
     * @param  Post $post
     * @return Illuminate\Http\Response
     */
    public function delete(Post $post) {

        $post->delete();
        return response()->json(['status' => 'record deleted successfully'], Response::HTTP_OK);

    }

}
