<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    use ApiResponse;

    /**
     * List comments
     *
     * @param  Post   $post
     * @return Illuminate\Http\Response
     */
    public function index(Post $post) {
        return response()->json($post->comments()->orderBy('created_at', 'desc')->paginate(), Response::HTTP_OK);
    }

    /**
     * Create comment
     *
     * @param  Post    $post
     * @param  Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Post $post, Request $request) {

        $data = $request->validate(['body' => 'required', 'parent_id' => 'numeric']);
        $data['creator_id'] = Auth::user()->id;

        return $this->success($post->comments()->save(new Comment($data)), Response::HTTP_CREATED);

    }

    /**
     * Update comment
     *
     * @param  Post    $post
     * @param  Comment $comment
     * @param  Request $request
     * @return Illuminate\Http\Response
     */
    public function update(Post $post, Comment $comment, Request $request) {

        $data = $request->validate(['body' => 'required', 'parent_id' => 'numeric']);
        $comment->fill($data)->save();

        return $this->success($comment, Response::HTTP_OK);

    }

    /**
     * Delete comment
     *
     * @param  Post    $post
     * @param  Comment $comment
     * @return Illuminate\Http\Response
     */
    public function delete(Post $post, Comment $comment) {

        $comment->delete();
        return response()->json(['status' => 'record deleted successfully'], Response::HTTP_OK);

    }

}
