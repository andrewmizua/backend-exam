<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponse;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Client;

class UserController extends Controller
{
    use ApiResponse;

    /**
     * User login
     *
     * @param  Request $request
     * @return Illuminate\Http\Response
     */
    public function login(Request $request) {

        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors'  => [
                    'email' => ['These credentials do not match our records.']
                ]
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user        = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token       = $tokenResult->token;

        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }

        $token->save();

        return response()->json([
            'token'      => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
        ]);

    }

    /**
     * User registration
     *
     * @param  Request $request
     * @return Illuminate\Http\Response
     */
    public function register(Request $request) {

        $request->validate([
            'name'     => 'required|string',
            'email'    => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed'
        ]);

        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        $user->save();

        return response()->json($user);

    }

    /**
     * User logout
     *
     * @param  Request $request
     * @return Illuminate\Http\Response
     */
    public function logout(Request $request) {
        $request->user()->token()->revoke();
        return $this->success('Successfully logged out', Response::HTTP_OK);
    }
}
