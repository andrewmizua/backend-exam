<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;

class CheckPostComment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // Verify if post and comment is equal
        if ($request->post->id === $request->comment->commentable_id) {
            return $next($request);
        }

        throw new ModelNotFoundException;

    }
}
