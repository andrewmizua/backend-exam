<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Post extends Model
{
    protected $fillable = [
        'title',
        'content',
        'slug',
        'user_id',
        'image'
    ];

    /**
     * Get post's user
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * Get all post's comments
     */
    public function comments() {
        return $this->morphMany('App\Comment', 'commentable');
    }

    /**
     * Create slug when setting title
     *
     * @param string $value
     */
    public function setTitleAttribute($value) {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = Str::slug($value);
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName() {
        return 'slug';
    }

}

