<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::middleware('auth:api')->group(function ($route) {

    $route->post('/posts/{post}/comments', 'CommentController@store');

    $route->middleware('post.comment')->group(function($route) {
        $route->patch('/posts/{post}/comments/{comment}', 'CommentController@update');
        $route->delete('/posts/{post}/comments/{comment}', 'CommentController@delete');
    });

    $route->post('/posts', 'PostController@store');
    $route->patch('/posts/{post}', 'PostController@update');
    $route->delete('/posts/{post}', 'PostController@delete');

    $route->post('/logout', 'UserController@logout');

});

Route::get('/posts/{post}/comments', 'CommentController@index');
Route::get('/posts/{post}', 'PostController@view');

Route::get('/posts', 'PostController@index');
Route::post('/register', 'UserController@register');
Route::post('/login', 'UserController@login');